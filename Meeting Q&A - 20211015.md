## Q&A - 2021-10-15

**1. 2 vs 4 cores per executor.**
- In general, higher cores per executor implies handling higher amount of tasks with lesser shuffling at a cost of  having lower memory per core and possibly more tasks failing if executor goes 'bad'.  
- Here, this [article](https://www.davidmcginnis.net/post/spark-job-optimization-myth-5-increasing-executor-cores-is-always-a-good-idea) tries to argue for 1 core per executor.
- The key idea behind is if your job already requires more memory that you're facing OOM at executor level, then decreasing cores instead of increasing executor memory could be a solution.
- However, I believe if you are confident your regular jobs' executors are consistent such that its not failing and being removed coupled with the fact that you've written efficient SQL code (or even increasing partition count to decrease partition size), maximising the amount of cores appears to be the better option (4 cores). With that said, I have yet to perform a comparison on performance for both configuration.

**2. Repartitioning vs not repartitioning due to an additional step which will inevitably increase runtime.**
- It is true that calling repartition explicitly may be expensive in terms of runtime as data undergoes a full shuffle.
- An alternative that surfaces commonly is to use coalesce to reduce and get the right amount of partitions, but I believe having too many partitions is often not the issue and we may unlikely meet our objectives of achieving full parallelism through increasing partition count as described in the slides.
- Personally, I do not use an explicit repartition call as well and would set default partition count by passing into the spark.sql.shuffle.partitions in config. I believe this might be faster than calling repartition(), but currently could not find resources to backup this claim yet as well.

**3. OR vs CASE in WHERE clause performance comparison.**
- I've performed this comparison with 2 simple test queries pulling 2 impressions on both, and the 'OR' SQL code goes OOM with multiple attempts.
- I've looked at the comparison between the DAGs and both looks similar. Ironically, it seems that the 'OR' code has lesser input data but still fails to run, appreciate any inputs if you have insights to this.
- [Repository](https://gitlab.com/tony.ngmk/spark-optimisation/-/tree/main/Experiment1)


**4. Can we reduce the value of R (storage memory that execution cannot evict)?**
- Yes, we can set this to a lower value via spark.driver.memory which has a default of 0.5.
- I've tried running scripts pulling data from CIV tables with spark.driver.memory at a value 0.1 pulling impression figures and have yet to encounter issues.
- However, I might have to experiment this longer with more 'demanding' queries before putting it up for recommendations.