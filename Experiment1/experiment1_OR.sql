WITH imp_table AS (
    SELECT 
        grass_region,
        DATE(datehour + interval '7' hour) AS grass_date,
        platform,
        page_type,
        COUNT(DISTINCT userid) AS user_count
    FROM shopee.traffic_mart_dwd__impression_di
    WHERE
        grass_region = 'ID'
        AND DATE(datehour + interval '7' hour) = DATE '{0}' - interval '2' day
        AND operation = 'impression'
        AND 
            (
            page_type = 'spl_home' 
            AND page_section[0] = 'pop_up_activation_success'
            )
            OR
            (
            page_type = 'cashloan_home' 
            AND page_section[0] = 'pop_up_activation_success'
            )   
    GROUP BY 1,2,3,4
)

SELECT * FROM imp_table