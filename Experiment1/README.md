# Experiment 1

Comparing `OR` vs. `CASE` within `WHERE` clause's performance from pulling impressions table.

- `CASE`: experiment_CASE.sql
- `OR`: experiment_OR.sql

|               | CASE                                                                                                                        | OR                                                                                                                        |
|---------------|-----------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------|
| SQL           | experiment1_CASE.sql                                                                                                        | experiment1_OR.sql                                                                                                        |
| DAG           | experiment1_CASE.pdf                                                                                                        | experiment1_OR.sql                                                                                                        |
| Spark-History | Link to [Case DAG](http://spark-history.data-infra.shopee.io/history/application_1632999510150_1805654/SQL/execution/?id=1) | Link to [OR DAG](http://spark-history.data-infra.shopee.io/history/application_1633605906188_1014509/SQL/execution/?id=2) |
| Time-taken    | 3.6 minutes                                                                                                                 | NIL                                                                                                                       |
| Status        | Completed                                                                                                                   | OOM                                                                                                                       |
--- 

## 1. Query

Both test queries are extracting :
- **spl_home** (pop_up_activation_success) - Impression
- **cashloan_home** (pop_up_activation_success) - Impression

## 2. Job config

Both test queries run with
- 4 cores per executor
- 512mb per executor

--- 

