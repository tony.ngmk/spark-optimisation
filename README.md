# Spark Optimisation Strategies

## Context
- Spark cluster manager’s queue appears to be in a constant state of high pressure 
- Applications and jobs appears to be running for a long time
- The amount of jobs will very possibly grow further in the future

## Tips overview
- Configure job configuration parameters (to use the right amount of resources, and possibly finish the job quickly to free up resources)
- Attempt to achieve full parallelism (fully using resources allocated)
- Reduce data skew (delaying resources from being freed up)
- Reduce runtime of applications (to quickly free-up resources)
- Reduce unnecessary scans (reduce the runtime and resources)